{$O-}

{$IFOPT O+}

Cannot overlay this module.

{$ELSE}

unit Async;

interface

type
    TCOMInfo    = record
                    { receive buffer pointers   }
                    InBufferSel     :       Word;
                    InBufferHead    :       Word;
                    InBufferTail    :       Word;
                    InBufferStart   :       Word;
                    InBufferEnd     :       Word;
                    { transmit buffer pointers  }
                    OutBufferSel    :       Word;
                    OutBufferHead   :       Word;
                    OutBufferTail   :       Word;
                    OutBufferStart  :       Word;
                    OutBufferEnd    :       Word;
                    { port parameters saved     }
                    { at library initialization }
                    { and restored at library   }
                    { cleanup                   }
                    RSSpeedDivLo    :       Byte;
                    RSSpeedDivHi    :       Byte;
                    RSIERReg        :       Byte;
                    RSLCRReg        :       Byte;
                    RSMCRReg        :       Byte;
                  end;

    TCOMRegs    = record
                    DivLo           :       Byte;
                    DivHi           :       Byte;
                    IOReg           :       Byte;
                    IERReg          :       Byte;
                    IIRReg          :       Byte;
                    LCRReg          :       Byte;
                    MCRReg          :       Byte;
                    LSRReg          :       Byte;
                    MSRReg          :       Byte;
                  end;

    PCOMInfo    = ^TCOMInfo;
    TStatusRegs  = record
                    MSR, LSR : Byte;
                  end;
    TServiceProc = procedure(
                              PortBase : Word;
                              PortInfo : PCOMInfo;
                              PortStat : TStatusRegs
                           );


function  GetCOMPort(Port : Byte) : Word;
{
******************************************************************************
* Returns port base address or zero if absent.                               *
******************************************************************************
}
function  GetAsyncError : Word;
{
******************************************************************************
* Returns internal error information and zeros it.                           *
* Zero means no error.                                                       *
******************************************************************************
}
function  SetBuffer(
                     PortNum : Byte;
                     IO      : Byte;
                     Buffer  : Pointer;
                     Size    : Word
                   ) : Integer;
{
******************************************************************************
* Sets transmit or receive buffer length and address.                        *
* PortNum - port number (1 - COM1, 2 - COM2, 3 - COM3, 4 - COM4)             *
* IO      - 0 for receive, 1 - for transmit buffer                           *
* Buffer  - far pointer to a buffer                                          *
* Size    - buffer size (below 64kB)                                         *
* Returns 0 for success, -1 for error                                        *
******************************************************************************
}
procedure ClearBuffer(PortNum : Byte; IO : Byte);
{
******************************************************************************
* Clears requested buffer.                                                   *
* PortNum - port number (1 - COM1, 2 - COM2, 3 - COM3, 4 - COM4)             *
* IO      - 0 for receive, 1 - for transmit buffer                           *
******************************************************************************
}
procedure SetLCRService(LCRService : TServiceProc);
{
******************************************************************************
* Sets address or procedure called when line error is detected.              *
* LCRService - procedure address                                             *
******************************************************************************
}
procedure SetMCRService(MCRService : TServiceProc);
{
******************************************************************************
* Sets address or procedure called when modem error is detected.             *
* MCRService - procedure address                                             *
******************************************************************************
}
function  OutputByte(PortNum : Byte; Data : Byte) : Integer;
{
******************************************************************************
* Inserts byte to transmit buffer of a given port.                           *
* PortNum - port number (1 - COM1, 2 - COM2, 3 - COM3, 4 - COM4)             *
* Data    - byte to send                                                     *
* Returns 0 for success, -1 for error                                        *
******************************************************************************
}
function  InputByte(PortNum : Byte) : Integer;
{
******************************************************************************
* Returns byte from receive buffer of a given port.                          *
* PortNum - port number (1 - COM1, 2 - COM2, 3 - COM3, 4 - COM4)             *
* Returns:                                                                   *
*   available byte from a buffer in lower byte of the result                 *
*   -1 if nothing is available                                               *
******************************************************************************
}
procedure InstallAsync;
{
******************************************************************************
* Installs interrupt handler, initializes internal state of the module.      *
* Must be called before any other function in this module.                   *
******************************************************************************
}
procedure UnInstallAsync;
{
******************************************************************************
* Restores interrupt handler and COM port settings.                          *
* Failing to call this procedure before living the program may hang a system *
* or result in unexpected behavior.                                          *
******************************************************************************
}
function  InBufStat(PortNum : Byte) : Word;
{
******************************************************************************
* Returns number of bytes available in receive buffer.                       *
* PortNum - port number (1 - COM1, 2 - COM2, 3 - COM3, 4 - COM4)             *
******************************************************************************
}
function  OutBufStat(PortNum : Byte) : Word;
{
******************************************************************************
* Returns number of bytes pending transmit in transmit buffer.               *
* PortNum - port number (1 - COM1, 2 - COM2, 3 - COM3, 4 - COM4)             *
******************************************************************************
}
procedure SetCOMRegs(PortNum : Byte; var COMRegs : TCOMRegs);
{
******************************************************************************
* Writes COMRegs content to appropriate COM port registers.                  *
******************************************************************************
}
procedure GetCOMRegs(PortNum : Byte; var COMRegs : TCOMRegs);
{
******************************************************************************
* Writes COM port registers to COMRegsto appropriate COMRegs fields.         *
******************************************************************************
}

procedure COMSetup(
                    PortNum : Byte;
                    Speed   : LongInt;
                    LCRReg  : Byte;
                    MCRReg  : Byte;
                    IERReg  : Byte
                  );
{
******************************************************************************
* Sets transmission parameters.                                              *
* PortNum - port number (1 - COM1, 2 - COM2, 3 - COM3, 4 - COM4)             *
* Speed   - transmission speed in bauds (2 - 115200)                         *
* LCRReg  - LCR contents                                                     *
* MCRReg  - MCR contents                                                     *
* IERReg  - IER contents                                                     *
* Note: the most significant bit of LCR regoster is always zeroed out.       *
******************************************************************************
}
procedure LockCOM(Mask : Byte);
{
******************************************************************************
* Blocks servicing of the given ports.                                       *
* Mask - bit mask (COM1 - - bit 0, COM2 - bit 1, COM3 - bit 2, COM4 - bit 3) *
*        setting the bit blocks servicing of the given COM port              *
******************************************************************************
}
procedure UnLockCOM(Mask : Byte);
{
******************************************************************************
* Unblocks servicing of the given ports.                                     *
* Mask - bit mask (COM1 - - bit 0, COM2 - bit 1, COM3 - bit 2, COM4 - bit 3) *
*        setting the bit unblocks servicing of the given COM port            *
* Note: this routine will not allow servicing of nonexistent port.           *
******************************************************************************
}

implementation

{$IFDEF DPMI}
  uses WinMisc;
{$ENDIF}
{$IFDEF WINDOWS}
  uses WinMisc;
{$ENDIF}

function GetCOMPort;
external;

function GetAsyncError;
external;

function SetBuffer;
external;

procedure SetLCRService;
external;

procedure SetMCRService;
external;

function OutputByte;
external;

function InputByte;
external;

procedure InstallAsync;
external;

procedure UnInstallAsync;
external;

function InBufStat;
external;

function OutBufStat;
external;

procedure SetCOMRegs;
external;

procedure GetCOMRegs;
external;

procedure COMSetup;
external;

procedure LockCOM;
external;

procedure UnLockCOM;
external;

procedure ClearBuffer;
external;

{$L async.obj}

{$ENDIF}

end.
