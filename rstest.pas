{$M 16384, 16384, 655360}
{$X+,V-}

program RSTest;

uses Async, App, Dialogs, Menus, Drivers, Objects, Views, Gadgets, Validate,
     MsgBox, Editors, StdDlg, TextView;

const
  cmSend           = $0400;
  cmReceive        = $0800;
  cmTrybPracy      = 1000;
  cmPortNadajnika  = 100;
  cmPortOdbiornika = 101;
  cmInicjacjaPortu = 102;
  cmHaslo          = 1003;
  cmZmianaHasla    = 1007;
  cmTransmisja     = 1008;
  cmClearAll       = 1001;
  cmClearBuffers   = 1002;
  hcSystem         = 0;
  hcDOSShell       = 0;
  hcQuit           = 0;
  hcOpcje          = 0;
  hcTrybPracy      = 0;
  hcPortNadajnika  = 0;
  hcPortOdbiornika = 0;
  hcInicjacjaPortu = 0;
  hcHaslo          = 0;
  hcZmianaHasla    = 0;
  hcTransmisja     = 0;
  hcTrZnak         = 0;
  hcTrTekst        = 0;
  hcTrPlik         = 0;
  hcCOMRegsDialog  = 0;
  hcOdbior         = 0;
  sHaslo           = 100;
  Haslo            = 'hahaha';

type
  PParamTextBis = ^TParamTextBis;
  TParamTextBis = object(TParamText)
    RegNo       : Integer;
    constructor Init(var Bounds : TRect; AText : String; AParamCount : Integer; ARegNo : Integer);
  end;
  PDialWindow = ^TDialWindow;
  TDialWindow = object(TWindow)
    procedure HandleEvent(var Event : TEvent); virtual;
  end;

  PSender = ^TSender;
  TSender = object(TMemo)
    procedure HandleEvent(var Event : TEvent); virtual;
  end;

  PReceiver = ^TReceiver;
  TReceiver = object(TMemo)
    procedure   HandleEvent(var Event : TEvent); virtual;
  end;

  PPetelka = ^TPetelka;
  TPetelka = object(TDialog)
    Receiver    : PReceiver;
    Sender      : PSender;
    constructor Init;
    procedure   HandleEvent(var Event: TEvent); virtual;
  end;

  PGetPassword   = ^TGetPassword;
  TGetPassword   = object(TInputLine)
    procedure   Draw; virtual;
  end;

  PCOMRegsDialog = ^TCOMRegsDialog;
  TCOMRegsDialog = object(TDialog)
    COMNo    : Byte;
    IO       : Byte;
    constructor Init(P : TPoint; ACOMNo : Byte; AIO : Byte);
    procedure   HandleEvent(var Event : TEvent); virtual;
    procedure   UpdateDisplay;
    procedure   SetPortNo(Port : Byte);
  end;

  PCOMApp = ^TCOMApp;
  TCOMApp = object(TApplication)
    TransmitterRegs : PCOMRegsDialog;
    TransmitterPort : Byte;
    ReceiverRegs    : PCOMRegsDialog;
    ReceiverPort    : Byte;
    WorkType        : Word;
    Clock           : PClockView;
    InBuf           : Pointer;
    OutBuf          : Pointer;
    constructor Init;
    destructor  Done; virtual;
    procedure   InitMenuBar; virtual;
    procedure   InitStatusLine; virtual;
    procedure   HandleEvent(var Event : TEvent); virtual;
    procedure   Idle; virtual;
    function    GetWorkType(var Rec) : Word;
    function    GetCOMNo(var Rec; var ATitle : string) : Word;
    function    GetCOMParams(com : Byte; IO : string; var Rec): Word;
    function    GetReceiver(var Rec) : Word;
    function    GetPassword(var Rec) : Word;
  end;

  TCOMParamsRec = record
    Speed       : Word;
    StopBits    : Word;
    DataBits    : Word;
    Parity      : Word;
    StuckParity : Word;
    Interrupts  : Word;
    Other       : Word;
  end;

  TCOMRegsRec = record
    IO  : LongInt;
    IER : LongInt;
    IIR : LongInt;
    LCR : LongInt;
    MCR : LongInt;
    LSR : LongInt;
    MSR : LongInt;
  end;

  PText2Send = ^TText2Send;
  TText2Send = record
    Length  : Word;
    Buffer  : array[0..1023] of Byte;
    Current : Word;
  end;

  PFileRecord = ^TFileRecord;
  TFileRecord = record
    f       : file;
    Current : Word;
    Length  : Word;
    Buffer  : array[0..8191] of Byte;
  end;

var
  COMApp       : TCOMApp;
  CharToSend   : Byte;
  Text2Send    : TText2Send;
  TermText     : Text;

const
  NadOdb       : array[0..1] of string[9] = ('Odbiornik', 'Nadajnik');
  COMs         : array[1..4] of string[1] = ('1', '2', '3', '4');
  ReTr         : array[0..1] of string[15] = ('Port odbiornika',
                                              'Port nadajnika');
  Rejestry     : array[0..6] of string[11] = ('Rejestr IO',
                                              'Rejestr IER',
                                              'Rejestr IIR',
                                              'Rejestr LCR',
                                              'Rejestr MCR',
                                              'Rejestr LSR',
                                              'Rejestr MSR');
  SendChar     : Boolean = False;
  AbortTr      : Boolean = False;
  IsReceiving  : Boolean = False;
  InputFile    : PFileRecord = nil;
  OutputFile   : PFileRecord = nil;

function Byte2Hex(Val : Byte) : string;
var
  Temp : Byte;
begin
  Temp := (Val and $F0) shr 4;
  if Temp < 10 then
    Byte2Hex[1] := Char(Temp + Byte('0'))
  else
    Byte2Hex[1] := Char(Temp + Byte('A') - 10);
  Temp := (Val and $0F);
  if Temp < 10 then
    Byte2Hex[2] := Char(Temp + Byte('0'))
  else
    Byte2Hex[2] := Char(Temp + Byte('A') - 10);
  Byte2Hex[0] := Char(2);
end;

function Hex2Byte(Str : string) : Byte;
var
  Temp1 : Byte;
  Temp2 : Byte;
begin
  if Byte(Str[0]) = 2 then
  begin
    case str[2] of
      'A'..'F' : Temp1 := Byte(str[2]) - Byte('A') + 10;
      'a'..'f' : Temp1 := Byte(str[2]) - Byte('a') + 10;
      '0'..'9' : Temp1 := Byte(str[2]) - Byte('0');
    end;
    case str[1] of
      'A'..'F' : Temp2 := (Byte(str[1]) - Byte('A') + 10) shl 4;
      'a'..'f' : Temp2 := (Byte(str[1]) - Byte('a') + 10) shl 4;
      '0'..'9' : Temp2 := (Byte(str[1]) - Byte('0')) shl 4;
    end;
    Hex2Byte := Temp1 + Temp2;
  end
  else
  begin
    case str[1] of
      'A'..'F' : Temp1 := (Byte(str[1]) - Byte('A') + 10);
      'a'..'f' : Temp1 := (Byte(str[1]) - Byte('a') + 10);
      '0'..'9' : Temp1 := (Byte(str[1]) - Byte('0'));
    end;
    Hex2Byte := Temp1;
  end;
end;

constructor TCOMApp.Init;
var
  R   : TRect;
  P   : TPoint;
  W   : Word;
begin
  HideMouse;
  MouseEvents := False;
  InstallAsync;
  inherited Init;
  GetMem(InBuf, 65520);
  GetMem(OutBuf, 8192);
  SetBuffer(1, 0, InBuf, 65520);
  SetBuffer(2, 0, InBuf, 65520);
  SetBuffer(3, 0, InBuf, 65520);
  SetBuffer(4, 0, InBuf, 65520);
  SetBuffer(1, 1, OutBuf, 8192);
  SetBuffer(2, 1, OutBuf, 8192);
  SetBuffer(3, 1, OutBuf, 8192);
  SetBuffer(4, 1, OutBuf, 8192);
  ReceiverRegs := nil;
  TransmitterRegs := nil;
  ReceiverPort := 1;
  TransmitterPort := 1;
  GetExtent(R);
  R.A.X := R.B.X - 9; R.B.Y := R.A.Y + 1;
  New(Clock, Init(R));
  Insert(Clock);
  DisableCommands([cmPortOdbiornika, cmPortNadajnika, cmInicjacjaPortu]);
  Text2Send.Length := 0;
end;

destructor TCOMApp.Done;
begin
  UninstallAsync;
  FreeMem(InBuf, 65520);
  FreeMem(OutBuf, 8192);
  inherited Done;
end;

procedure TCOMApp.InitMenuBar;
Var R:TRect;
Begin
  GetExtent(R);
  R.B.Y:=R.A.Y+1;
  MenuBar:=New(PMenuBar, Init(R, NewMenu(
    NewSubMenu('~S~ystem', hcSystem, NewMenu(
      NewItem('~D~OS shell', '', kbNoKey, cmDOSShell, hcDOSShell,
      NewItem('~K~oniec', 'Alt-X', kbAltX, cmQuit, hcQuit, nil))),
    NewSubMenu('O~p~cje', hcOpcje, NewMenu(
      NewItem('~T~ryb pracy', 'Alt-T', kbAltT, cmTrybPracy, hcTrybPracy,
      NewItem('Port ~n~adajnika', 'Alt-N', kbAltN, cmPortNadajnika, hcPortNadajnika,
      NewItem('Port ~o~dbiornika', 'Alt-O', kbAltO, cmPortOdbiornika, hcPortOdbiornika,
      NewItem('~I~nicjacja portu', 'Alt-I', kbAltI, cmInicjacjaPortu, hcInicjacjaPortu,
      NewItem('~H~aslo', 'Alt-H', kbAltH, cmHaslo, hcHaslo, nil)))))),
    NewItem('T~r~ansmisja', '',kbAltR, cmTransmisja, hcTransmisja, nil))))));
end;

procedure TCOMApp.InitStatusLine;
var
  R : TRect;
begin
  GetExtent(R);
  R.A.Y := R.B.Y - 1;
  StatusLine := New(PStatusLine, Init(R,
    NewStatusDef(0, 65535,
      NewStatusKey('~Alt-X~ Koniec', kbAltX, cmQuit,
      NewStatusKey('~F6~ Nastepne okno', kbF6, cmNext,
      NewStatusKey('~F10~ Menu', kbF10, cmMenu,
    nil))),
  nil)));
end;

procedure TCOMApp.HandleEvent(var Event : TEvent);
var
  A         : PPetelka;
  Rec       : TCOMParamsRec;
  CI        : TCOMRegs;
  P         : TPoint;
  W, C, N   : Word;
  H,H1      : string;
function Nadawanie(var Rec): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
begin
  R.Assign(25, 5, 56, 16);
  D := New(PDialog, Init(R, 'Nadaj'));
  with D^ do
    begin
      Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
      HelpCtx := hcNoContext;
      R.Assign(10, 2, 21, 5);
      V := New(PRadioButtons, Init(R,
        NewSItem('~Z~nak',
        NewSItem('~T~ekst',
        NewSItem('~P~lik',
        nil)))));
      Insert(V);
      R.Assign(4, 7, 14, 9);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(17, 7, 27, 9);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  Nadawanie := ExecuteDialog(D, @Rec);
end;
function Odbieranie(var Rec): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
begin
  R.Assign(20, 5, 60, 15);
  D := New(PDialog, Init(R, 'Odbierz'));
  with D^ do
    begin
      Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
      HelpCtx := hcNoContext;
      R.Assign(10, 2, 31, 4);
      V := New(PRadioButtons, Init(R,
        NewSItem('Do ~o~kna',
        NewSItem('Do ~p~liku i okna',
        nil))));
      Insert(V);
      R.Assign(7, 6, 17, 8);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(23, 6, 33, 8);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  Odbieranie := ExecuteDialog(D, @Rec);
end;
function Petla(var Rec): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
begin
  R.Assign(23, 5, 56, 15);
  D := New(PDialog, Init(R, 'Petla'));
  with D^ do
    begin
      Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
      HelpCtx := hcNoContext;
      R.Assign(11, 2, 22, 4);
      V := New(PRadioButtons, Init(R,
        NewSItem('~P~lik',
        NewSItem('~T~ekst',
        nil))));
      Insert(V);
      R.Assign(5, 6, 15, 8);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(18, 6, 28, 8);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  Petla := ExecuteDialog(D, @Rec);
end;
function ZnakDoWysl(var Rec): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
  S: string[2];
  W: Word;
begin
  R.Assign(27, 3, 53, 16);
  D := New(PDialog, Init(R, 'Znak do wyslania'));
  with D^ do
    begin
      Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
      HelpCtx := hcNoContext;
      R.Assign(11, 3, 15, 4);
      V := New(PInputLine, Init(R, 2));
      PInputLine(V)^.SetValidator(New(PFilterValidator, Init(['0'..'9','A'..'F','a'..'f'])));
      Insert(V);
      R.Assign(8, 6, 18, 8);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(8, 9, 18, 11);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  S := '';
  ZnakDoWysl := ExecuteDialog(D, @S);
  Word(Rec) := Hex2Byte(S);
end;
function TekstDoNadania(var Rec): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
begin
  R.Assign(19, 0, 58, 23);
  D := New(PDialog, Init(R, 'Tekst do nadania'));
  with D^ do
    begin
      HelpCtx := hcNoContext;
      R.Assign(6, 2, 33, 19);
      V := New(PMemo, Init(R, nil, nil, nil, 1024));
      Insert(V);
      R.Assign(7, 20, 17, 22);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(22, 20, 32, 22);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  TekstDoNadania := ExecuteDialog(D, @Rec);
end;
function Odbiornik : PDialog;
var
  D                      : PDialog;
  W                      : PDialWindow;
  R                      : TRect;
  HScrollBar, VScrollBar : PScrollBar;
  Terminal               : PTerminal;
begin
  Desktop^.GetExtent(R);
  New(D, Init(R, ''));
  D^.GetExtent(R);
  R.Grow(-1, -1); Dec(R.B.Y, 3);
  New(W, Init(R, 'Odbiornik', wnNoNumber));
  W^.SetState(sfShadow, False); W^.Flags := 0;
  W^.GetExtent(R);
  R.Grow(-1, -1);
  HScrollBar := W^.StandardScrollBar(sbHorizontal or sbHandleKeyboard);
  VScrollBar := W^.StandardScrollBar(sbVertical or sbHandleKeyboard);
  New(Terminal, Init(R, HScrollBar, VScrollBar, 32768));
  AssignDevice(TermText, Terminal);
  Rewrite(TermText);
  W^.Insert(Terminal);
  D^.Insert(W);
  D^.GetExtent(R);
  Dec(R.B.Y, 1); R.A.Y := R.B.Y - 2;
  R.A.X := (R.B.X - 10) div 2; R.B.X := R.A.X + 10;
  D^.Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
  Odbiornik := D;
end;

begin
  inherited HandleEvent(Event);
  if (Event.What = evBroadcast) then
  begin
    case Event.Command of
      cmSend:
      begin
        repeat
        until OutputByte(TransmitterPort, Event.InfoByte) = 0;
      end;
      cmClearAll:
      begin
        LockCOM($F);
        ClearBuffer(TransmitterPort, 1);
        ClearBuffer(ReceiverPort, 0);
        UnLockCOM(Byte((1 shl (TransmitterPort - 1)) or (1 shl (ReceiverPort - 1))));
      end;
      else
        Exit;
    end;
    ClearEvent(Event);
  end
  else
    if Event.What = evCommand then
    begin
      case Event.Command of
        cmTransmisja:
        begin
          W := 0;
          case WorkType of
            1:
            begin
              if Odbieranie(W) = cmOk then
              begin
                if W = 1 then
                begin
                  H := '*.*';
                  if ExecuteDialog(New(PFileDialog,
                     Init(H, 'Plik docelowy',
                    '~N~azwa', fdOkButton, 100)), @H) <> cmCancel then
                  begin
                    New(OutputFile);
                    with OutputFile^ do
                    begin
                      Assign(f, H);
                      Rewrite(f, 1);
                      Current := 0;
                    end;
                  end;
                end;
                ClearBuffer(ReceiverPort, 0);
                UnlockCOM(1 shl (ReceiverPort - 1));
                IsReceiving := True;
                ExecuteDialog(Odbiornik, nil);
                LockCOM($F);
                IsReceiving := False;
                if OutputFile <> nil then
                begin
                  with OutputFile^ do
                  begin
                    BlockWrite(f, Buffer, Current);
                    Close(f);
                  end;
                  Dispose(OutputFile);
                  OutputFile := nil;
                end;
              end;
            end;
            2:
            begin
              if Nadawanie(W) = cmOk then
              begin
                case W of
                  0:
                  begin
                    if ZnakDoWysl(W) = cmOk then
                    begin
                      SendChar := True;
                      CharToSend := Byte(W);
                      ClearBuffer(TransmitterPort, 1);
                      UnLockCOM(1 shl (TransmitterPort - 1));
                      MessageBox(^C'Trwa transmisja'^M^C'Cancel konczy nadawanie',
                        nil, mfInformation or mfCancelButton);
                      LockCOM($F);
                      SendChar := False;
                    end;
                  end;
                  1:
                  begin
                    Text2Send.Length := 0;
                    if TekstDoNadania(Text2Send) = cmOk then
                    begin
                      ClearBuffer(TransmitterPort, 1);
                      UnLockCOM(1 shl (TransmitterPort - 1));
                      Text2Send.Current := 0;
                      AbortTr := False;
                      AbortTr := cmCancel = MessageBox(
                        ^C'Trwa transmisja'^M^C'Cancel konczy nadawanie',
                        nil, mfInformation or mfCancelButton);
                    end;
                  end;
                  2:
                  begin
                    H := '*.*';
                    if ExecuteDialog(New(PFileDialog,
                       Init(H, 'Plik do wyslania',
                      '~N~azwa', fdOkButton, 100)), @H) <> cmCancel then
                    begin
                      New(InputFile);
                      with InputFile^ do
                      begin
                        Assign(f, H);
                        Reset(f, 1);
                        BlockRead(f, Buffer, 8192, Length);
                        Current := 0;
                      end;
                      ClearBuffer(TransmitterPort, 1);
                      UnLockCOM(1 shl (TransmitterPort - 1));
                      AbortTr := False;
                      AbortTr := cmCancel = MessageBox(
                        ^C'Trwa transmisja'^M^C'Cancel konczy nadawanie',
                        nil, mfInformation or mfCancelButton);
                    end;
                  end;
                end
              end;
            end;
            3:
            begin
              ClearBuffer(TransmitterPort, 1);
              ClearBuffer(ReceiverPort, 0);
              UnLockCOM(Byte((1 shl (TransmitterPort - 1)) or (1 shl (ReceiverPort - 1))));
              IsReceiving := True;
              ExecuteDialog(New(PPetelka, Init), nil);
              IsReceiving := False;
              LockCOM($F);
              TransmitterRegs^.UpdateDisplay;
              ReceiverRegs^.UpdateDisplay;
            end;
          end;
        end;
        cmTrybPracy:
        begin
          if WorkType = 0 then
            W := 0
          else
            W := WorkType - 1;
          if GetWorkType(W) = cmOk then
          begin
            WorkType := W + 1;
            case W of
              0:
              begin
                EnableCommands([cmPortOdbiornika]);
                DisableCommands([cmPortNadajnika]);
                P.X := 25; P.Y := 2;
                if TransmitterRegs <> nil then
                begin
                  DeskTop^.Delete(TransmitterRegs);
                  Dispose(TransmitterRegs, Done);
                  TransmitterRegs := nil;
                end;
                if ReceiverRegs = nil then
                begin
                  ReceiverRegs := New(PCOMRegsDialog, Init(P, ReceiverPort, 0));
                  InsertWindow(ReceiverRegs);
                end
                else
                  ReceiverRegs^.MoveTo(P.X, P.Y);
              end;
              1:
              begin
                DisableCommands([cmPortOdbiornika]);
                EnableCommands([cmPortNadajnika]);
                P.X := 25; P.Y := 2;
                if ReceiverRegs <> nil then
                begin
                  DeskTop^.Delete(ReceiverRegs);
                  Dispose(ReceiverRegs, Done);
                  ReceiverRegs := nil;
                end;
                if TransmitterRegs = nil then
                begin
                  TransmitterRegs := New(PCOMRegsDialog, Init(P, TransmitterPort, 1));
                  InsertWindow(TransmitterRegs);
                end
                else
                  TransmitterRegs^.MoveTo(P.X, P.Y);
              end;
              2:
              begin
                EnableCommands([cmPortOdbiornika, cmPortNadajnika]);
                P.X := 45; P.Y := 2;
                if ReceiverRegs = nil then
                begin
                  ReceiverRegs := New(PCOMRegsDialog, Init(P, ReceiverPort, 0));
                  InsertWindow(ReceiverRegs);
                end
                else
                  ReceiverRegs^.MoveTo(P.X, P.Y);
                Dec(P.X, 40);
                if TransmitterRegs = nil then
                begin
                  TransmitterRegs := New(PCOMRegsDialog, Init(P, TransmitterPort, 1));
                  InsertWindow(TransmitterRegs);
                end
                else
                  TransmitterRegs^.MoveTo(P.X, P.Y);
                TransmitterRegs^.Focus;
              end;
            end;
          end;
        end;
        cmPortNadajnika:
        begin
          W := TransmitterPort - 1;
          if GetCOMNo(W, ReTr[1]) = cmOk then
          begin
            TransmitterPort := W + 1;
            TransmitterRegs^.SetPortNo(TransmitterPort);
          end;
        end;
        cmPortOdbiornika:
        begin
          W := ReceiverPort - 1;
          if GetCOMNo(W, ReTr[0]) = cmOk then
          begin
            ReceiverPort := W + 1;
            ReceiverRegs^.SetPortNo(ReceiverPort);
          end;
        end;
        cmInicjacjaPortu:
        begin
          if (DeskTop^.Current <> nil) and
             ((DeskTop^.Current = PView(ReceiverRegs)) or
              (DeskTop^.Current = PView(TransmitterRegs))) then
          begin
            C := PCOMRegsDialog(DeskTop^.Current)^.COMNo;
            N := PCOMRegsDialog(DeskTop^.Current)^.IO;
            GetCOMRegs(C, CI);
            with Rec do
              with CI do
              begin
                case DivHi of
                  $09: if DivLo = 0 then Speed := 0 else Speed := $FFFF;
                  $04: if DivLo = $17 then Speed := 1 else Speed := $FFFF;
                  $03: if DivLo = 0 then Speed := 2 else Speed := $FFFF;
                  $01: if DivLo = $80 then Speed := 3 else Speed := $FFFF;
                  $00:
                  case DivLo of
                    $C0: Speed := 4;
                    $60: Speed := 5;
                    $39: Speed := 6;
                    $30: Speed := 7;
                    $18: Speed := 8;
                    $10: Speed := 9;
                    $0C: Speed := 10;
                    $06: Speed := 11;
                    $03: Speed := 12;
                    $02: Speed := 13;
                    $01: Speed := 14;
                    else Speed := $FFFF;
                  end;
                  else Speed := $FFFF;
                end;
                if LCRReg and $04 = 0 then StopBits := 0 else StopBits := 1;
                DataBits := LCRReg and $03;
                if (LCRReg and $08) = 0 then
                  Parity := 0
                else
                  if (LCRReg and $10) = 0 then
                    Parity := 2
                  else
                    Parity := 1;
                if (LCRReg and $20) = 0 then
                  StuckParity := 0
                else
                  StuckParity := 1;
                Interrupts := IERReg and $0F;
                Other := 0;
                if (LCRReg and $40) <> 0 then
                  Other := Other or $01;
                if (MCRReg and $10) <> 0 then
                  Other := Other or $02;
                if (MCRReg and $08) <> 0 then
                  Other := Other or $04;
              end;
            if GetCOMParams(C, ReTr[N], Rec) <> cmCancel then
            begin
              with Rec do
                with CI do
                begin
                  case Speed of
                    00:
                    begin
                      DivLo := $00;
                      DivHi := $09;
                    end;
                    01:
                    begin
                      DivLo := $17;
                      DivHi := $04;
                    end;
                    02:
                    begin
                      DivLo := $00;
                      DivHi := $03;
                    end;
                    03:
                    begin
                      DivLo := $80;
                      DivHi := $01;
                    end;
                    04:
                    begin
                      DivLo := $C0;
                      DivHi := $00;
                    end;
                    05:
                    begin
                      DivLo := $60;
                      DivHi := $00;
                    end;
                    06:
                    begin
                      DivLo := $39;
                      DivHi := $00;
                    end;
                    07:
                    begin
                      DivLo := $30;
                      DivHi := $00;
                    end;
                    08:
                    begin
                      DivLo := $18;
                      DivHi := $00;
                    end;
                    09:
                    begin
                      DivLo := $10;
                      DivHi := $00;
                    end;
                    10:
                    begin
                      DivLo := $0C;
                      DivHi := $00;
                    end;
                    11:
                    begin
                      DivLo := $06;
                      DivHi := $00;
                    end;
                    12:
                    begin
                      DivLo := $03;
                      DivHi := $00;
                    end;
                    13:
                    begin
                      DivLo := $02;
                      DivHi := $00;
                    end;
                    14:
                    begin
                      DivLo := $01;
                      DivHi := $00;
                    end;
                  end;
                  LCRReg := 0;
                  if StopBits = 1 then
                    LCRReg := LCRReg or $04;
                  LCRReg := LCRReg or DataBits;
                  if Parity = 1 then
                    LCRReg := LCRReg or $18
                  else
                    if Parity = 2 then
                      LCRReg := LCRReg or $08;
                  if StuckParity <> 0 then
                    LCRReg := LCRReg or $20;
                  IERReg := Interrupts;
                  MCRReg := 0;
                  if (Other and $01) <> 0 then
                    LCRReg := LCRReg or $40;
                  if (Other and $02) <> 0 then
                    MCRReg := MCRReg or $10;
                  if (Other and $04) <> 0 then
                    MCRReg := MCRReg or $08;
                end;
              SetCOMRegs(C, CI);
              PCOMRegsDialog(DeskTop^.Current)^.UpdateDisplay;
            end;
          end;
        end;
        cmHaslo:
        begin
          while True do
          begin
            H := '';
            if GetPassword(H) <> cmCancel then
            begin
              if H = Haslo then
              begin
                EnableCommands([cmInicjacjaPortu]);
                Break;
              end
              else
                if MessageBox('Zle haslo. Czy chcesz powtorzyc ?', nil,
                   mfInformation or mfYesButton or mfNoButton) = cmYes then
                  Continue
                else
                  Break;
            end
            else
              Break;
          end;
        end;
        else
          Exit;
      end;
      ClearEvent(Event);
    end;
end;

procedure TCOMApp.Idle;
var
  Event : TEvent;
  A     : Integer;
begin
  inherited Idle;
  Clock^.Update;
  if not IsReceiving then
  begin
    if TransmitterRegs <> nil then
      TransmitterRegs^.UpdateDisplay;
    if ReceiverRegs <> nil then
      ReceiverRegs^.UpdateDisplay;
  end;
  if SendChar then
    OutputByte(TransmitterPort, CharToSend);
  with Text2Send do
    if Length <> 0 then
      if AbortTr then
      begin
        Length := 0;
        LockCOM($F);
      end
      else
        if Current < Length then
        begin
          if OutputByte(TransmitterPort, Buffer[Current]) = 0 then
            Inc(Current);
        end
        else
        begin
          with Event do
          begin
            What := evCommand;
            Command := cmCancel;
          end;
          PutEvent(Event);
        end;
  if InputFile <> nil then
    with InputFile^ do
    begin
      if AbortTr then
      begin
        Close(f);
        LockCOM($f);
        Dispose(InputFile);
        InputFile := nil;
      end
      else
        if Current = Length then
          if Length = 8192 then
          begin
            BlockRead(f, Buffer, 8192, Length);
            Current := 0;
          end
          else
          begin
            with Event do
            begin
              What := evCommand;
              Command := cmCancel;
            end;
            PutEvent(Event);
          end
        else
          if OutputByte(TransmitterPort, Buffer[Current]) = 0 then
            Inc(Current);
    end;
  if IsReceiving then
  begin
    A := InputByte(ReceiverPort);
    if A <> -1 then
    begin
      if OutputFile <> nil then
        with OutputFile^ do
        begin
          if Current > 8190 then
          begin
            BlockWrite(f, Buffer, Current + 1);
            Current := 0;
          end;
          Buffer[Current] := Byte(A);
          Inc(Current);
        end;
      Message(Application, evBroadcast, cmReceive, Pointer(A));
    end;
  end;
end;

function TCOMApp.GetWorkType(var Rec): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
begin
  R.Assign(20, 5, 60, 16);
  D := New(PDialog, Init(R, 'Tryb pracy'));
  with D^ do
    begin
      Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
      HelpCtx := hcNoContext;
      R.Assign(7, 3, 33, 6);
      V := New(PRadioButtons, Init(R,
        NewSItem('~O~dbiornik',
        NewSItem('~N~adajnik',
        NewSItem('Odbiornik ~i~ nadajnik',
        nil)))));
      Insert(V);
      R.Assign(7, 8, 17, 10);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(23, 8, 33, 10);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  GetWorkType := ExecuteDialog(D, @Rec);
end;

function TCOMApp.GetCOMNo(var Rec; var ATitle : string): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
begin
  R.Assign(23, 5, 56, 15);
  D := New(PDialog, Init(R, ATitle));
  with D^ do
    begin
      Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
      HelpCtx := hcNoContext;
      R.Assign(11, 2, 22, 6);
      V := New(PRadioButtons, Init(R,
        NewSItem('COM ~1~',
        NewSItem('COM ~2~',
        NewSItem('COM ~3~',
        NewSItem('COM ~4~',
        nil))))));
      Insert(V);
      R.Assign(5, 7, 15, 9);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(18, 7, 28, 9);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  GetCOMNo := ExecuteDialog(D, @Rec);
end;

function TCOMApp.GetCOMParams(com : Byte; IO : string; var Rec): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
  S: String[1];
begin
  R.Assign(8, 0, 72, 23);
  Str(com : 1, S);
  D := New(PDialog, Init(R, 'Parametry transmisji - ' + IO + ' - COM ' + S));
  with D^ do
    begin
      Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
      HelpCtx := hcNoContext;
      R.Assign(3, 3, 18, 18);
      V := New(PRadioButtons, Init(R,
        NewSItem('50',
        NewSItem('110',
        NewSItem('150',
        NewSItem('300',
        NewSItem('600',
        NewSItem('1200',
        NewSItem('2000',
        NewSItem('2400',
        NewSItem('4800',
        NewSItem('7200',
        NewSItem('9600',
        NewSItem('19200',
        NewSItem('38400',
        NewSItem('56000',
        NewSItem('115200',
        nil)))))))))))))))));
      Insert(V);
      R.Assign(2, 2, 12, 3);
      Insert(New(PLabel, Init(R, '~S~zybkosc:', V)));
      R.Assign(3, 19, 18, 21);
      V := New(PRadioButtons, Init(R,
        NewSItem('1',
        NewSItem('1,5 lub 2',
        nil))));
      Insert(V);
      R.Assign(2, 18, 14, 19);
      Insert(New(PLabel, Init(R, 'Bity s~t~opu:', V)));
      R.Assign(21, 3, 49, 7);
      V := New(PRadioButtons, Init(R,
        NewSItem('5 bitow',
        NewSItem('6 bitow',
        NewSItem('7 bitow',
        NewSItem('8 bitow',
        nil))))));
      Insert(V);
      R.Assign(20, 2, 33, 3);
      Insert(New(PLabel, Init(R, '~B~ity danych:', V)));
      R.Assign(21, 8, 49, 11);
      V := New(PRadioButtons, Init(R,
        NewSItem('brak parzystosci',
        NewSItem('parzystosc',
        NewSItem('nieparzystosc',
        nil)))));
      Insert(V);
      R.Assign(20, 7, 32, 8);
      Insert(New(PLabel, Init(R, '~P~arzystosc:', V)));
      R.Assign(21, 11, 49, 12);
      V := New(PCheckBoxes, Init(R,
        NewSItem('parzystosc ~w~ymuszona',
        nil)));
      Insert(V);
      R.Assign(21, 13, 49, 17);
      V := New(PCheckBoxes, Init(R,
        NewSItem('znak do odebrania',
        NewSItem('rejestr nadawczy pusty',
        NewSItem('stan linii',
        NewSItem('stan modemu',
        nil))))));
      Insert(V);
      R.Assign(20, 12, 32, 13);
      Insert(New(PLabel, Init(R, 'P~r~zerwania:', V)));
      R.Assign(21, 18, 49, 21);
      V := New(PCheckBoxes, Init(R,
        NewSItem('przerwanie transmisji',
        NewSItem('wewnetrzne sprzezenie',
        NewSItem('przerwania dopuszczone',
        nil)))));
      Insert(V);
      R.Assign(20, 17, 26, 18);
      Insert(New(PLabel, Init(R, '~I~nne:', V)));
      R.Assign(51, 8, 61, 10);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(51, 14, 61, 16);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  GetCOMParams := ExecuteDialog(D, @Rec);
end;

function TCOMApp.GetReceiver(var Rec): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
begin
  R.Assign(20, 5, 56, 17);
  D := New(PDialog, Init(R, 'dummy'));
  with D^ do
    begin
      Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
      HelpCtx := hcNoContext;
      R.Assign(9, 3, 27, 5);
      V := New(PRadioButtons, Init(R,
        NewSItem('~O~kna',
        NewSItem('~P~liku i okna',
        nil))));
      Insert(V);
      R.Assign(8, 2, 19, 3);
      Insert(New(PLabel, Init(R, 'O~d~bior do:', V)));
      R.Assign(9, 7, 27, 8);
      V := New(PInputLine, Init(R, 127));
      Insert(V);
      R.Assign(8, 6, 21, 7);
      Insert(New(PLabel, Init(R, '~N~azwa pliku:', V)));
      R.Assign(6, 9, 16, 11);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(20, 9, 30, 11);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  GetReceiver := ExecuteDialog(D, @Rec);
end;

function TCOMApp.GetPassword(var Rec) : Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
begin
  R.Assign(20, 5, 56, 13);
  D := New(PDialog, Init(R, 'Podaj haslo'));
  with D^ do
    begin
      Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
      HelpCtx := hcNoContext;
      R.Assign(12, 2, 24, 3);
      V := New(PGetPassword, Init(R, 255));
      Insert(V);
      R.Assign(6, 5, 16, 7);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(20, 5, 30, 7);
      Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  GetPassword := ExecuteDialog(D, @Rec);
end;

constructor TCOMRegsDialog.Init(P : TPoint; ACOMNo : Byte; AIO : Byte);
var
  Header : string[30];
  R      : TRect;
  V      : PView;
begin
  R.Assign(P.X, P.Y, P.X + 30, P.Y + 11);
  Header := NadOdb[AIO] + ' - COM ' + COMs[ACOMNo];
  inherited Init(R, Header);
  COMNo := ACOMNo;
  IO := AIO;
  Options := ofSelectable + ofTopSelect + ofBuffered + ofVersion20;
  HelpCtx := hcCOMRegsDialog;
  R.Assign(20, 2, 24, 3);
  V := New(PParamTextBis, Init(R, ' %02X ', 1, 0));
  Insert(V);
  R.Assign(5, 2, 16, 3);
  Insert(New(PLabel, Init(R, Rejestry[0], V)));
  R.Assign(20, 3, 24, 4);
  V := New(PParamTextBis, Init(R, ' %02X ', 1, 1));
  Insert(V);
  R.Assign(5, 3, 17, 4);
  Insert(New(PLabel, Init(R, Rejestry[1], V)));
  R.Assign(20, 4, 24, 5);
  V := New(PParamTextBis, Init(R, ' %02X ', 1, 2));
  Insert(V);
  R.Assign(5, 4, 17, 5);
  Insert(New(PLabel, Init(R, Rejestry[2], V)));
  R.Assign(20, 5, 24, 6);
  V := New(PParamTextBis, Init(R, ' %02X ', 1, 3));
  Insert(V);
  R.Assign(5, 5, 17, 6);
  Insert(New(PLabel, Init(R, Rejestry[3], V)));
  R.Assign(20, 6, 24, 7);
  V := New(PParamTextBis, Init(R, ' %02X ', 1, 4));
  Insert(V);
  R.Assign(5, 6, 17, 7);
  Insert(New(PLabel, Init(R, Rejestry[4], V)));
  R.Assign(20, 7, 24, 8);
  V := New(PParamTextBis, Init(R, ' %02X ', 1, 5));
  Insert(V);
  R.Assign(5, 7, 17, 8);
  Insert(New(PLabel, Init(R, Rejestry[5], V)));
  R.Assign(20, 8, 24, 9);
  V := New(PParamTextBis, Init(R, ' %02X ', 1, 6));
  Insert(V);
  R.Assign(5, 8, 17, 9);
  Insert(New(PLabel, Init(R, Rejestry[6], V)));
  SelectNext(False);
  UpdateDisplay;
end;

procedure TCOMRegsDialog.UpdateDisplay;
var
  COMRegs : TCOMRegs;
  Rec     : TCOMRegsRec;
begin
  GetCOMRegs(COMNo, COMRegs);
  Rec.MSR := COMRegs.MSRReg;
  Rec.LSR := COMRegs.LSRReg;
  Rec.MCR := COMRegs.MCRReg;
  Rec.LCR := COMRegs.LCRReg;
  Rec.IIR := COMRegs.IIRReg;
  if (COMRegs.LCRReg and $80) <> 0 then
  begin
    Rec.IO  := COMRegs.DivLo;
    Rec.IER := COMRegs.DivHi;
  end
  else
  begin
    Rec.IO  := COMRegs.IOReg;
    Rec.IER := COMRegs.IERReg;
  end;
  SetData(Rec);
  DrawView;
end;

procedure TCOMRegsDialog.SetPortNo(Port : Byte);
begin
  DisposeStr(Title);
  COMNo := Port;
  Title := NewStr(NadOdb[IO] + ' - COM ' + COMs[COMNo]);
  UpdateDisplay;
  Redraw;
end;

procedure TCOMRegsDialog.HandleEvent(var Event : TEvent);
function GetNew(Etykieta : string; var Rec): Word;
var
  R: TRect;
  D: PDialog;
  V: PView;
begin
  R.Assign(24, 5, 56, 12);
  D := New(PDialog, Init(R, 'Nowa wartosc'));
  with D^ do
    begin
      HelpCtx := hcNoContext;
      R.Assign(5, 2, 16, 3);
      Insert(New(PStaticText, Init(R, Etykieta)));
      R.Assign(22, 2, 26, 3);
      V := New(PInputLine, Init(R, 2));
      PInputLine(V)^.SetValidator(New(PFilterValidator, Init(['A'..'F','a'..'f','0'..'9'])));
      Insert(V);
      R.Assign(4, 4, 14, 6);
      Insert(New(PButton, Init(R, '  O~K~  ', cmOK, bfDefault)));
      R.Assign(17, 4, 27, 6);
      Insert(New(PButton, Init(R, 'Cancel', cmCancel, bfNormal)));
      SelectNext(False);
    end;
  GetNew := Application^.ExecuteDialog(D, @Rec);
end;
var
  Displ : Word;
  S     : string[2];

begin
  if (Event.What = evKeyDown) and (Event.KeyCode = kbEnter) then
  begin
    Displ := PParamTextBis(Current)^.RegNo;
    S := Byte2Hex(Port[GetCOMPort(COMNo) + Displ]);
    if GetNew(Rejestry[Displ], S) <> cmCancel then
    begin
      Port[GetCOMPort(COMNo) + Displ] := Hex2Byte(S);
      UpdateDisplay;
    end;
    ClearEvent(Event);
  end
  else
    inherited HandleEvent(Event);
end;

procedure TGetPassword.Draw;
var
  CurData : PString;
begin
  CurData := Data;
  GetMem(Data, MaxLen + 1);
  FillChar(Data^[1], Byte(CurData^[0]), '*');
  Data^[0] := CurData^[0];
  inherited Draw;
  FreeMem(Data, Maxlen + 1);
  Data := CurData;
end;

procedure TSender.HandleEvent(var Event : TEvent);
var
  AEvent : TEvent;
begin
  if (Event.What = evBroadcast) and (Event.Command = cmClearAll) then
  begin
    SetSelect(0, BufLen, False);
    DeleteSelect;
  end
  else
  begin
    if (Event.What = evKeyDown) and (Event.ScanCode <> 15) then
    begin
      Message(Application, evBroadcast, cmSend, Pointer(Event.ScanCode));
      Message(Application, evBroadcast, cmSend, Pointer(Event.CharCode));
    end;
    inherited HandleEvent(Event);
  end;
end;

procedure TReceiver.HandleEvent(var Event : TEvent);
var
  AEvent : TEvent;
const
  A      : Integer = -1;
begin
  if (Event.What = evKeyDown) and (Event.ScanCode <> 15) then
    ClearEvent(Event)
  else
    if (Event.What = evBroadcast) and (Event.Command = cmReceive) then
      if A = -1 then
        A := Word(Event.InfoByte) shl 8
      else
      begin
        AEvent.What := evKeyDown;
        AEvent.KeyCode := Event.InfoByte or A;
        inherited HandleEvent(AEvent);
        ClearEvent(Event);
        A := -1;
      end
    else
      if (Event.What = evBroadcast) and (Event.Command = cmClearAll) then
      begin
        SetSelect(0, BufLen, False);
        DeleteSelect;
      end
      else
        inherited HandleEvent(Event);
end;

constructor TPetelka.Init;
var
  R: TRect;
  V: PView;
begin
  R.Assign(15, 0, 64, 23);
  inherited Init(R, 'Petla');
  Options := ofSelectable+ofTopSelect+ofBuffered+ofVersion20;
  HelpCtx := hcNoContext;
  R.Assign(6, 3, 31, 11);
  V := New(PSender, Init(R, nil, nil, nil, 1024));
  Insert(V);
  Sender := PSender(V);
  R.Assign(5, 2, 23, 3);
  Insert(New(PLabel, Init(R, 'Tekst do ~n~adania:', V)));
  R.Assign(6, 13, 31, 21);
  V := New(PReceiver, Init(R, nil, nil, nil, 1024));
  Insert(V);
  Receiver := PReceiver(V);
  R.Assign(5, 12, 21, 13);
  Insert(New(PLabel, Init(R, 'Tekst ~o~debrany:', V)));
  R.Assign(32, 11, 43, 13);
  Insert(New(PButton, Init(R, '~W~yczysc', cmClearAll, bfNormal)));
  R.Assign(32, 17, 44, 19);
  Insert(New(PButton, Init(R, '~C~ancel', cmCancel, bfNormal)));
  SelectNext(False);
end;

procedure TPetelka.HandleEvent;
begin
  if (Event.What = evCommand) and (Event.Command = cmClearAll) then
  begin
    Message(Application, evBroadcast, cmClearAll, nil);
    ClearEvent(Event);
  end
  else
    inherited HandleEvent(Event);
end;

procedure TDialWindow.HandleEvent;
begin
  if (Event.What = evBroadcast) and (Event.Command = cmReceive) then
  begin
    Write(TermText, Event.InfoChar);
    ClearEvent(Event);
  end
  else
    inherited HandleEvent(Event);
end;

constructor TParamTextBis.Init;
begin
  inherited Init(Bounds, AText, AParamCount);
  Options := Options or ofSelectable;
  RegNo := ARegNo;
end;

begin
  COMApp.Init;
  COMApp.Run;
  COMApp.Done;
end.
