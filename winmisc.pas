unit WinMisc;

interface

procedure __0000h;
procedure __0040h;
procedure __A000h;
procedure __B000h;
procedure __B800h;
procedure __C000h;
procedure __D000h;
procedure __E000h;
procedure __F000h;
procedure __AHINCR;
procedure __AHSHIFT;
procedure __GP;
procedure __ROMBIOS;
procedure __WINFLAGS;

implementation

procedure __0000h;    external 'KERNEL' index 183;
procedure __0040h;    external 'KERNEL' index 193;
procedure __A000h;    external 'KERNEL' index 174;
procedure __B000h;    external 'KERNEL' index 181;
procedure __B800h;    external 'KERNEL' index 182;
procedure __C000h;    external 'KERNEL' index 195;
procedure __D000h;    external 'KERNEL' index 179;
procedure __E000h;    external 'KERNEL' index 190;
procedure __F000h;    external 'KERNEL' index 194;
procedure __AHINCR;   external 'KERNEL' index 114;
procedure __AHSHIFT;  external 'KERNEL' index 113;
procedure __GP;       external 'KERNEL' index 342;
procedure __ROMBIOS;  external 'KERNEL' index 173;
procedure __WINFLAGS; external 'KERNEL' index 178;

end.
