.pas.exe:
	bpc -$G- -$N- -$D- $<

.pas.tpu:
	bpc -$G- -$N- -$D- $<

.asm.obj:
	tasm -m -mu -d__p__ $<

rstest.exe: rstest.pas async.tpu gadgets.tpu stddlg.tpu

async.tpu: async.pas async.obj

async.obj: async.asm macros.inc

clean:
	del *.obj
	del *.tpu

all: rstest.exe clean
