IDEAL
INCLUDE "macros.inc"
JUMPS

PUBLIC  _CType  GetCOMPort
PUBLIC  _CType  GetAsyncError
PUBLIC  _CType  SetBuffer
PUBLIC  _CType  OutputByte
PUBLIC  _CType  InputByte
PUBLIC  _CType  InstallAsync
PUBLIC  _CType  UnInstallAsync
PUBLIC  _CType  InBufStat
PUBLIC  _CType  OutBufStat
PUBLIC  _CType  SetCOMRegs
PUBLIC  _CType  GetCOMRegs
PUBLIC  _CType  COMSetup
PUBLIC  _CType  LockCOM
PUBLIC  _CType  UnLockCOM
PUBLIC  _CType  SetLCRService
PUBLIC  _CType  SetMCRService
PUBLIC  _CType  ClearBuffer

STRUC           TCOMInfo
InBufferSel     dw      ?
InBufferHead    dw      ?
InBufferTail    dw      ?
InBufferStart   dw      ?
InBufferEnd     dw      ?
OutBufferSel    dw      ?
OutBufferHead   dw      ?
OutBufferTail   dw      ?
OutBufferStart  dw      ?
OutBufferEnd    dw      ?
RSSpeedDivLo    db      ?
RSSpeedDivHi    db      ?
RSIERReg        db      ?
RSLCRReg        db      ?
RSMCRReg        db      ?
ENDS

STRUC           TCOMRegs
DivLo           db      ?
DivHi           db      ?
IOReg           db      ?
IERReg          db      ?
IIRReg          db      ?
LCRReg          db      ?
MCRReg          db      ?
LSRReg          db      ?
MSRReg          db      ?
ENDS

UDATASEG
ISRFlags        db      ?
AsyncError      db      ?
COM1Data        TCOMInfo        <>
COM2Data        TCOMInfo        <>
COM3Data        TCOMInfo        <>
COM4Data        TCOMInfo        <>
OldInt0Bh       dd      ?
OldInt0Ch       dd      ?
OldPICMask      db      ?
label           MCRService      CODEPTR
                db      ((@CodeSize + 1) * 2) dup (?)
label           LCRService      CODEPTR
                db      ((@CodeSize + 1) * 2) dup (?)

CODESEG
PROC _CType     GetCOMPort
                ARG     @@port : BYTE
                mov     bl,[@@port]
                dec     bl
                xor     bh,bh
                shl     bx,1
                mov     ax,Sel0040h
                mov     es,ax
                mov     ax,[es:bx]
                ret
ENDP

PROC _CType     GetAsyncError
                SetUpDS
                xor     ax,ax
                xchg    [AsyncError],al
                ret
ENDP

PROC _CType     SetBuffer
                ARG     @@COM : BYTE, @@io : BYTE, @@ptr : DWORD, @@size : WORD
                SetUpDS
                mov     al,[@@COM]
                dec     al
                mov     ah,SIZE TCOMInfo
                mul     ah
                mov     bx,offset COM1Data
                add     bx,ax
                mov     ax,[word @@ptr]
                mov     dx,[word @@ptr+2]
                mov     cx,[@@size]
                add     cx,ax
                dec     cx
                cmp     cx,ax
                ja      @@1
                mov     ax,-1
                jmp     @@4
        @@1:    cli
                cmp     [@@io],0
                jne     @@2
                mov     [(TCOMInfo PTR bx).InBufferSel],dx
                mov     [(TCOMInfo PTR bx).InBufferStart],ax
                mov     [(TCOMInfo PTR bx).InBufferHead],ax
                mov     [(TCOMInfo PTR bx).InBufferTail],ax
                mov     [(TCOMInfo PTR bx).InBufferEnd],cx
                jmp     @@3
        @@2:    mov     [(TCOMInfo PTR bx).OutBufferSel],dx
                mov     [(TCOMInfo PTR bx).OutBufferStart],ax
                mov     [(TCOMInfo PTR bx).OutBufferHead],ax
                mov     [(TCOMInfo PTR bx).OutBufferTail],ax
                mov     [(TCOMInfo PTR bx).OutBufferEnd],cx
        @@3:    sti
                mov     ax,0
        @@4:    ret
ENDP

PROC _CType     ClearBuffer
                ARG     @@ComNo : BYTE, @@io : BYTE
                SetUpDS
                mov     al,[@@ComNo]
                dec     al
                mov     ah,SIZE TCOMInfo
                mul     ah
                mov     bx,offset COM1Data
                add     bx,ax
                cmp     [@@io],0
                jne     @@1
                mov     ax,[(TCOMInfo PTR bx).InBufferStart]
                mov     [(TCOMInfo PTR bx).InBufferHead],ax
                mov     [(TCOMInfo PTR bx).InBufferTail],ax
                jmp     @@2
        @@1:    mov     ax,[(TCOMInfo PTR bx).OutBufferStart]
                mov     [(TCOMInfo PTR bx).OutBufferHead],ax
                mov     [(TCOMInfo PTR bx).OutBufferTail],ax
        @@2:    ret
ENDP

PROC _CType     SetMCRService
                ARG     @@service : CODEPTR
                SetUpDS
                mov     ax,[word @@service]
IF @CodeSize gt 0
                mov     dx,[word @@service+2]
                mov     [word MCRService+2],dx
ENDIF
                mov     [word MCRService],ax
                ret
ENDP

PROC _CType     SetLCRService
                ARG     @@service : CODEPTR
                SetUpDS
                mov     ax,[word @@service]
IF @CodeSize gt 0
                mov     dx,[word @@service+2]
                mov     [word LCRService+2],dx
ENDIF
                mov     [word LCRService],ax
                ret
ENDP

PROC _CType     OutputByte
                ARG     @@p : BYTE, @@d : BYTE
                SetUpDS
                mov     ax,Sel0040h
                mov     es,ax
                mov     bl,[@@p]
                xor     bh,bh
                dec     bx
                mov     al,bl
                shl     bx,1
                mov     dx,[es:bx]              ; base address for port @@p
                mov     ah,SIZE TCOMInfo
                mul     ah
                mov     bx,offset COM1Data
                add     bx,ax                   ; data for port @@p
                mov     ax,[(TCOMInfo PTR bx).OutBufferHead]
                cmp     ax,[(TCOMInfo PTR bx).OutBufferEnd]
                jne     @@1
                mov     cx,[(TCOMInfo PTR bx).OutBufferStart]
                jmp     @@2
        @@1:    mov     cx,ax
                inc     cx
        @@2:    cmp     cx,[(TCOMInfo PTR bx).OutBufferTail]
                jne     @@3
                mov     ax,-1
                jmp     @@4
        @@3:    mov     [(TCOMInfo PTR bx).OutBufferHead],cx
                mov     es,[(TCOMInfo PTR bx).OutBufferSel]
                mov     bx,ax
                mov     al,[@@d]
                mov     [es:bx],al
                inc     dx
                cli
                in      al,dx
                or      al,2
                out     dx,al
                sti
                xor     ax,ax
        @@4:    ret
ENDP

PROC _CType     InputByte
                ARG     @@p : BYTE
                SetUpDS
                mov     al,[@@p]
                dec     al
                mov     ah,SIZE TCOMInfo
                mul     ah
                mov     bx,offset COM1Data
                add     bx,ax
                mov     ax,[(TCOMInfo PTR bx).InBufferTail]
                cmp     ax,[(TCOMInfo PTR bx).InBufferHead]
                jne     @@1
                mov     ax,-1
                jmp     @@4
        @@1:    cmp     ax,[(TCOMInfo PTR bx).InBufferEnd]
                je      @@2
                mov     cx,ax
                inc     cx
                jmp     @@3
        @@2:    mov     cx,[(TCOMInfo PTR bx).InBufferStart]
        @@3:    mov     [(TCOMInfo PTR bx).InBufferTail],cx
                mov     es,[(TCOMInfo PTR bx).InBufferSel]
                mov     bx,ax
                mov     al,[es:bx]
                xor     ah,ah
        @@4:    ret
ENDP

PROC _CType     InstallAsync
                SaveSI
                SetUpDS
                mov     ax,350Bh
                int     21h
                mov     [word OldInt0Bh],bx
                mov     [word OldInt0Bh+2],es
                mov     al,0Ch
                int     21h
                mov     [word OldInt0Ch],bx
                mov     [word OldInt0Ch+2],es
                push    ds
                push    cs
                pop     ds
                mov     dx,offset Int0Bh
                mov     ax,250Bh
                int     21h
                mov     dx,offset Int0Ch
                mov     al,0Ch
                int     21h
                pop     ds
                mov     [ISRFlags],0
                mov     [AsyncError],0
                cli
                in      al,21h
                mov     [OldPICMask],al
                and     al,0E7h
                out     21h,al
                sti
                mov     ax,Sel0040h
                mov     es,ax
                mov     cx,4
                mov     si,0
                mov     bx,offset COM1Data
        @@1:    mov     dx,[es:si]              ; port base
                or      dx,dx
                jz      @@4
        @@2:    in      al, dx
                add     dx,5
                in      al, dx
                inc     dx
                in      al, dx
                sub     dx,4
                in      al, dx
                cmp     al, 1
                je      @@3
                sub     dx,2
                jmp     @@2
        @@3:    inc     dx                      ; DX -> LCR
                cli
                in      al,dx
                mov     [(TCOMInfo PTR bx).RSLCRReg],al
                or      al,80h
                out     dx,al
                sub     dx,3
                in      ax,dx
                mov     [(TCOMInfo PTR bx).RSSpeedDivLo],al
                mov     [(TCOMInfo PTR bx).RSSpeedDivHi],ah
                add     dx,3
                xor     al,al
                out     dx,al
                sub     dx,2
                in      al,dx
                mov     [(TCOMInfo PTR bx).RSIERReg],al
                add     dx,3
                in      al,dx
                mov     [(TCOMInfo PTR bx).RSMCRReg],al
                mov     al,[(TCOMInfo PTR bx).RSLCRReg]
                dec     dx
                out     dx,al
                sti
        @@4:    add     si,2
                add     bx,SIZE TCOMInfo
                loop    @@1
                ret
ENDP

PROC _CType     UnInstallAsync
                SaveSI
                SetUpDS
                push    ds
                les     bx,[OldInt0Ch]
                lds     dx,[OldInt0Bh]
                mov     ax,250Bh
                int     21h
                mov     dx,bx
                push    es
                pop     ds
                mov     al,0Ch
                int     21h
                pop     ds
                mov     al,[OldPICMask]
                out     21h,al
                mov     ax,Sel0040h
                mov     es,ax
                mov     cx,4
                mov     si,0
                mov     bx,offset COM1Data
        @@1:    mov     dx,[es:si]              ; port base
                or      dx,dx
                jz      @@2
                add     dx,3                    ; DX -> LCR
                cli
                mov     al,80h
                out     dx,al
                sub     dx,3
                mov     al,[(TCOMInfo PTR bx).RSSpeedDivLo]
                mov     ah,[(TCOMInfo PTR bx).RSSpeedDivHi]
                out     dx,ax
                add     dx,3
                xor     al,al
                out     dx,al
                sub     dx,2
                mov     al,[(TCOMInfo PTR bx).RSIERReg]
                out     dx,al
                add     dx,2
                mov     al,[(TCOMInfo PTR bx).RSLCRReg]
                mov     ah,[(TCOMInfo PTR bx).RSMCRReg]
                out     dx,ax
                sti
        @@2:    add     si,2
                add     bx,SIZE TCOMInfo
                loop    @@1
                ret
ENDP

PROC _CType     InBufStat
                ARG     @@com : BYTE
                SetUpDS
                mov     al,[@@com]
                dec     al
                mov     ah,SIZE TCOMInfo
                mul     ah
                mov     bx,offset COM1Data
                add     bx,ax
                mov     ax,[(TCOMInfo PTR bx).InBufferHead]
                sub     ax,[(TCOMInfo PTR bx).InBufferTail]
                jge     @@1
                mov     dx,[(TCOMInfo PTR bx).InBufferEnd]
                sub     dx,[(TCOMInfo PTR bx).InBufferStart]
                add     ax,dx
        @@1:    ret
ENDP

PROC _CType     OutBufStat
                ARG     @@com : BYTE
                SetUpDS
                mov     al,[@@com]
                dec     al
                mov     ah,SIZE TCOMInfo
                mul     ah
                mov     bx,offset COM1Data
                add     bx,ax
                mov     ax,[(TCOMInfo PTR bx).OutBufferHead]
                sub     ax,[(TCOMInfo PTR bx).OutBufferTail]
                jge     @@1
                mov     dx,[(TCOMInfo PTR bx).OutBufferEnd]
                sub     dx,[(TCOMInfo PTR bx).OutBufferStart]
                add     ax,dx
        @@1:    ret
ENDP

PROC _CType     SetCOMRegs
                ARG     @@Port : BYTE, @@ci : DATAPTR
                SaveSI
                cld
                mov     ax,Sel0040h
                mov     es,ax
                mov     bl,[@@Port]
                dec     bl
                xor     bh,bh
                shl     bx,1
                mov     dx,[es:bx]
                LoadES
                LES_    si,[@@ci]
                add     dx,3
                mov     al,80h
                cli
                out     dx,al
                sub     dx,3
IF (@Cpu AND 1Fh) EQ 1                          ; 8086 only
                lods    [byte es:si]
                out     dx,al
                inc     dx
                lods    [byte es:si]
                out     dx,al
                add     dx,2
                xor     al,al
                out     dx,al
                sub     dx,3
                mov     cx,7
        @@1:    lods    [byte es:si]
                out     dx,al
                inc     dx
                loop    @@1
ELSE
                outs    dx,[byte es:si]
                inc     dx
                outs    dx,[byte es:si]
                add     dx,2
                xor     al,al
                out     dx,al
                sub     dx,3
                mov     cx,7
        @@1:    outs    dx,[byte es:si]
                inc     dx
                loop    @@1
ENDIF
                sti
                ret
ENDP

PROC _CType     GetCOMRegs
                SaveDI
                ARG     @@Port : BYTE, @@ci : DATAPTR
                cld
                mov     ax,Sel0040h
                mov     es,ax
                mov     bl,[@@Port]
                dec     bl
                xor     bh,bh
                shl     bx,1
                mov     dx,[es:bx]
                LoadES
                LES_    di,[@@ci]
                add     dx,3
                cli
                in      al,dx
                mov     ah,al
                mov     al,80h
                out     dx,al
                mov     cx,3
                sub     dx,cx
IF (@Cpu AND 1Fh) EQ 1                          ; 8086 only
                in      al,dx
                stosb
                inc     dx
                in      al,dx
                stosb
                add     dx,2
                xor     al,al
                out     dx,al
                sub     dx,cx
        @@1:    in      al,dx
                stosb
                inc     dx
                loop    @@1
                mov     al,ah
                out     dx,al
                stosb
                mov     cx,3
        @@2:    inc     dx
                in      al,dx
                stosb
                loop    @@2
ELSE
                insb
                inc     dx
                insb
                add     dx,2
                xor     al,al
                out     dx,al
                sub     dx,cx
        @@1:    insb
                inc     dx
                loop    @@1
                mov     al,ah
                out     dx,al
                stosb
                mov     cx,3
        @@2:    inc     dx
                insb
                loop    @@2
ENDIF
                sti
                ret
ENDP

PROC _CType     COMSetup
                ARG     @@COM : BYTE, @@Speed : DWORD, @@LCR : BYTE, \
                        @@MCR : BYTE, @@IER : BYTE
                SaveSI
                SaveDI
                push    bp
                mov     cx,32
                mov     bx,[word @@Speed]
                mov     bp,[word @@Speed+2]
                mov     ax,0C200h               ; magic number
                mov     dx,1                    ; magic number
                xor     si,si
                xor     di,di
        @@1:    shl     ax,1
                rcl     dx,1
                rcl     si,1
                rcl     di,1
                cmp     di,bp
                jb      @@3
                ja      @@2
                cmp     si,bx
                jb      @@3
        @@2:    sub     si,bx
                sbb     di,bp
                inc     ax
        @@3:    loop    @@1
                pop     bp                      ; divisor in AX
                mov     di,ax
                mov     bx,Sel0040h
                mov     es,bx
                mov     bl,[@@COM]
                dec     bl
                xor     bh,bh
                shl     bx,1
                mov     dx,[es:bx]              ; port base
                add     dx,3
                mov     al,80h
                cli
                out     dx,al
                sub     dx,3
                mov     ax,di
                out     dx,ax
                add     dx,3
                mov     al,[@@LCR]
                and     al,7Fh
                mov     ah,[@@MCR]
                out     dx,ax
                sub     dx,2
                mov     al,[@@IER]
                out     dx,al
                sti
                ret
ENDP

PROC _CType     LockCOM
                ARG     @@mask : BYTE
                SetUpDS
                mov     al,[@@mask]
                not     al
                and     al,0Fh
                and     [ISRFlags],al
                ret
ENDP

PROC _CType     UnLockCOM
                ARG     @@mask : BYTE
                SetUpDS
                mov     al,[@@mask]
                and     al,0Fh
                mov     bx,Sel0040h
                mov     es,bx
                mov     bx,6
                mov     ah,0F7h
        @@1:    cmp     [word es:bx],0          ; port base
                jne     @@2
                and     al,ah
        @@2:    sar     ah,1
                sub     bx,2
                jns     @@1
                or      [ISRFlags],al
                ret
ENDP

                ASSUME  ds : nothing, es : nothing, ss : nothing
Int0Bh:         push    es
                push    ax
                push    bx
                push    dx
                mov     ax,Sel0040h
                mov     es,ax
                mov     dx,[es:2]               ; COM2 base
                or      dx,dx
                jz      @@1
                add     dx,2
                in      al,dx
                cmp     al,1
                je      @@1
                mov     ah,2
                mov     bx,offset COM2Data
                jmp     ISRCommon
        @@1:    mov     dx,[es:6]               ; COM4 base
                or      dx,dx
                jz      ISREnd1
                add     dx,2
                in      al,dx
                mov     ah,8
                mov     bx,offset COM4Data
                jmp     ISRCommon
Int0Ch:         push    es
                push    ax
                push    bx
                push    dx
                mov     ax,Sel0040h
                mov     es,ax
                mov     dx,[es:0]               ; COM1 base
                or      dx,dx
                jz      @@1
                add     dx,2
                in      al,dx
                cmp     al,1
                je      @@1
                mov     ah,1
                mov     bx,offset COM1Data
                jmp     ISRCommon
        @@1:    mov     dx,[es:4]               ; COM3 base
                or      dx,dx
                jz      ISREnd1
                add     dx,2
                in      al,dx
                cmp     al,1
                mov     ah,4
                mov     bx,offset COM3Data
ISRCommon:      sub     dx,2
                cmp     al,1
                je      ISREnd1
                push    ds
IF @Model eq 1
                push    cs
                pop     ds
ELSE
                push    ax
                mov     ax,@data
                mov     ds,ax
                pop     ax
ENDIF
                ASSUME  ds : @data
                test    ah,[ISRFlags]
                pushf
                cmp     al,2
                je      ISRSend
                cmp     al,4
                je      ISRReceive
                cmp     al,0
                je      ISRMCRService
                cmp     al,6
                je      ISRLCRService
ISRReceive:     add     dx,3
                in      al,dx
                mov     ah,al
                and     al,7Fh
                out     dx,al
                sub     dx,3
                in      al,dx
                xchg    al,ah
                add     dx,3
                out     dx,al
                sub     dx,3
                mov     al,ah
                popf
                jz      ISREnd2
                push    di
                push    si
                ASSUME  es : nothing
                mov     es,[(TCOMInfo ptr bx).InBufferSel]
                mov     di,[(TCOMInfo ptr bx).InBufferHead]
                cmp     di,[(TCOMInfo ptr bx).InBufferEnd]
                jne     @@1
                mov     si,[(TCOMInfo ptr bx).InBufferStart]
                jmp     @@2
        @@1:    lea     si,[di+1]
        @@2:    cmp     si,[(TCOMInfo ptr bx).InBufferTail]
                je      @@3
                mov     [(TCOMInfo ptr bx).InBufferHead],si
                mov     [es:di],al
                jmp     ISREnd4
        @@3:    mov     [AsyncError],1
                jmp     ISREnd4
ISRSend:        popf
                push    di
                jz      @@2
                mov     es,[(TCOMInfo ptr bx).OutBufferSel]
                mov     di,[(TCOMInfo ptr bx).OutBufferTail]
                cmp     di,[(TCOMInfo ptr bx).OutBufferHead]
                je      @@2
                add     dx,3
                in      al,dx
                mov     ah,al
                and     al,7Fh
                out     dx,al
                sub     dx,3
                mov     al,[es:di]
                out     dx,al
                add     dx,3
                mov     al,ah
                out     dx,al
                sub     dx,3
                cmp     di,[(TCOMInfo ptr bx).OutBufferEnd]
                je      @@1
                inc     [(TCOMInfo ptr bx).OutBufferTail]
                jmp     ISREnd3
        @@1:    mov     di,[(TCOMInfo ptr bx).OutBufferStart]
                mov     [(TCOMInfo ptr bx).OutBufferTail],di
                jmp     ISREnd3
        @@2:    inc     dx
                in      al,dx
                and     al,0FDh
                out     dx,al
                dec     dx
                jmp     ISREnd3
ISRMCRService:  add     dx,5
                in      ax,dx
                xchg    al,ah
                sub     dx,5
                popf
                jz      ISREnd2
IF @CodeSize eq 0
                cmp     [MCRService],0
ELSE
                push    ax
                mov     ax,[word MCRService]
                or      ax,[word MCRService+2]
                pop     ax
ENDIF
                jz      ISREnd2
                push    di
                push    si
                push    cx
                push    dx
                push    bp
                call    [MCRService] _CType, dx, $$ES bx, ax
                jmp     ISREnd5
ISRLCRService:  add     dx,5
                in      ax,dx
                xchg    al,ah
                sub     dx,5
                popf
                jz      ISREnd2
IF @CodeSize eq 0
                cmp     [LCRService],0
ELSE
                push    ax
                mov     ax,[word LCRService]
                or      ax,[word LCRService+2]
                pop     ax
ENDIF
                jz      ISREnd2
                push    di
                push    si
                push    cx
                push    dx
                push    bp
                call    [LCRService] _CType, dx, $$ES bx, ax
ISREnd5:        pop     bp
                pop     dx
                pop     cx
ISREnd4:        pop     si
ISREnd3:        pop     di
ISREnd2:        pop     ds
                inc     dx
                in      al,dx
                mov     ah,al
                xor     al,al
                out     dx,al
                mov     al,ah
                out     dx,al
ISREnd1:        pop     dx
                pop     bx
                mov     al,20h
                out     20h,al
                pop     ax
                pop     es
                iret
END
